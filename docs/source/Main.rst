============
Introduction
============


What it is
==================

This software is meant to improve the useability of permafrost data by allowing data creators to visually flag, assess, and take action on suspicious measurements. It includes a library of functions for identifying suspicious data, and a GUI to assist in the cleaning of the data. The reasons for creating tempcf are as follows:

- Fully-automated data cleaning is great, but isn't always the right choice
- The best person to assess whether a data point is `'real'` is probably the person most familiar with the site


tempcf is designed to encourage contributions from the community. It is written in Python, rather than as a web app because the language is more accessible and relevant to the scientists who will likely use and contribute to it. Similarly, tkinter is not the cleanest GUI interface, but it comes installed with most Python distributions and therefore requires the least burden on users of the software. 

The library of functions that identify suspicious data points can be used as a standalone resource.


What it isn't
=============

There are existing tools for data cleaning that may suit your needs better than this one.

`MeteoIO <https://www.slf.ch/en/services-and-products/meteoio.html>`_ : MeteoIO aims to make data access easy and safe for numerical simulations in environmental sciences requiring general meteorological data. It is robust, well-maintained, and used in data production pipelines at large organizations including the  `WMO Global Cryosphere Watch <https://gcw.met.no/node/10>`_. However, it is fully-automated, offering less control on what data is removed and what is retained, and can be difficult to use for those not comfortable with programming (although a `python interface <https://models.slf.ch/p/meteoio/source/tree/HEAD/trunk/tools/pythonWrapper>`_ does exist). tempcf is not a production-level data pipeline.

`Permadata Data Integration Tool <https://github.com/PermaData/DIT/>`_ is designed to homogenize data from multiple sources with different data structures, units, and nomenclature and perform basic automated data cleaning. On the other hand, tempcf is designed to work with data that is already homogenized, and it is meant to be a more in-depth tool for improving the quality of the data. tempcf is not something that will gather up disparate data files and homogenize them.

Installing
==========

The easiest way to get tempcf is to download the latest files from the `Releases <https://gitlab.com/permafrostnet/permafrost-tempcf/-/releases>`_ page of the GitLab repository. This requires no coding experience whatsoever, but only works on Windows and Linux at the moment (sorry Mac users!).

If you are familiar with python and would rather run tempf from the source code, follow the instructions below. This is the preferred option if you intend to contribute, or if you want to make any modifications.


Step 1: Download the files
--------------------------

```cmd
git clone --recurse-submodules https://gitlab.com/permafrostnet/permafrost-tempcf
```

Step 2: Create a virtual environment
------------------------------------
This makes sure that the python packages that get installed don't conflict with other python packages you might have installed.
On Mac or Linux, make sure to replace '\' with '/' in the last line.

```cmd
cd permafrost-tempcf
python -m venv env
.\env\Scripts\activate 
```

Step 3: Install tempcf
----------------------
These commands will install the required dependencies, and launch tempcf

```cmd
python setup.py install
cd tempcf
python main.py
```


