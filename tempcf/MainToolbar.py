from functools import partial
from pathlib import Path

import re
import tkinter as tk
import tkinter.filedialog as filedialog

from tempcf.HelpDialog import HelpDialog
from tempcf.Observable import Observable
from tempcf.MetadataDialog import MetadataDialog
from tempcf.Utils import resourcePath
from tempcf.AboutDialog import AboutDialog
from tempcf.FilterConfigure import FilterConfigure
from tempcf.FilterContainer import FilterContainer
import tempcf.FileTypes as ft


class MainToolbar(tk.Frame):
    # A dictionary of export options that are enabled, dependent on the type of dataset opened
    _FULL_DATA_OPTIONS = {
        "wide": [0, 3],
        ft.NTGS: [0, 3],
        ft.GTNP: [0, 3],
        "netcdf": [],
        ft.GEOPREC: [0, 1, 2, 3],
        ft.HOBO: [0, 3, 4],
        ft.RBR: [0, 3, 4],
        ft.GSC: [4],
        ft.VEMCO: [0, 3, 4],
        "database": []
    }

    _SINGLE_CHANNEL_OPTIONS = {
        "wide": [],
        ft.NTGS: [],
        ft.GTNP: [],
        "netcdf": [],
        ft.GEOPREC: [],
        ft.HOBO: [4],
        ft.RBR: [4],
        ft.GSC: [4],
        ft.VEMCO: [4],
        "database": []
    }

    def __init__(self, parent):
        self.filepath = Observable(None)
        self.exportPath = Observable(None)
        self.logPath = Observable(None)
        self.parent = parent
        self.toolbarObservable = Observable()
        self.program_menu = tk.Menu(parent)
        self.file_dropdown = tk.Menu(self.program_menu, tearoff=0)
        self.filters_dropdown = tk.Menu(self.program_menu, tearoff=0)
        self.depths_dropdown = tk.Menu(self.program_menu, tearoff=0)
        self.context_data_dropdown = tk.Menu(self.program_menu, tearoff=0)
        self.context_data_type_dropdown = tk.Menu(self.context_data_dropdown, tearoff=0)
        self.file_open_type_dropdown = tk.Menu(self.file_dropdown, tearoff=0)
        self.export_type_dropdown = tk.Menu(self.file_dropdown, tearoff=0)
        self.export_all_channels = tk.Menu(self.export_type_dropdown, tearoff=0)
        self.export_one_channel = tk.Menu(self.export_type_dropdown, tearoff=0)
        self.help_dropdown = tk.Menu(self.program_menu, tearoff=0)
        self._last_save_file = None
        self._last_open_file = None

        # Order matters here! The order in which these menu options are added correspond to the indices in "_AVAILABLE_OPTIONS"
        # so that they can be toggled easily depending on what is selected. If you change the order here, change the index in the dictionary as well.
        # The array of indices are the export options available, which is dependent on the import option selected.

        # File Menu
        self.file_open_type_dropdown.add_command(label="NTGS Database (CSV)", command=partial(self.openFile, ft.NTGS))
        self.file_open_type_dropdown.add_command(label="GTN-P Database", command=partial(self.openFile, ft.GTNP))
        self.file_open_type_dropdown.add_command(label="GeoPrecision (CSV)", command=partial(self.openFile, ft.GEOPREC))
        self.file_open_type_dropdown.add_command(label="HOBO (HOBOware CSV)", command=partial(self.openFile, ft.HOBO))
        self.file_open_type_dropdown.add_command(label="RBR (DAT, hex, rsk, xls(x))",
                                                 command=partial(self.openFile, ft.RBR))
        self.file_open_type_dropdown.add_command(label="Vemco (000, csv)", command=partial(self.openFile, ft.VEMCO))
        self.file_open_type_dropdown.add_command(label="GSC cleaned raw (CSV)", command=partial(self.openFile, ft.GSC))
        self.file_open_type_dropdown.add_command(label="Database", state=tk.DISABLED)

        self.export_type_dropdown.add_cascade(label="Export all channels", menu=self.export_all_channels)
        self.export_all_channels.add_command(label="NTGS Database", state=tk.DISABLED,
                                             command=partial(self.saveFile, ft.NTGS, False))  # Export Option 0
        self.export_all_channels.add_command(label="GeoPrecision (GP5W CSV)", state=tk.DISABLED,
                                             command=partial(self.saveFile, ft.GP5W, False))  # Export Option 1
        self.export_all_channels.add_command(label="GeoPrecision (FG2 CSV)", state=tk.DISABLED,
                                             command=partial(self.saveFile, ft.FG2, False))  # Export Option 2
        self.export_all_channels.add_command(label="GTN-P Database", state=tk.DISABLED,
                                             command=partial(self.saveFile, ft.GTNP, False))  # Export Option 3
        self.export_all_channels.add_command(label="GSC cleaned raw", state=tk.DISABLED,
                                             command=partial(self.saveFile, ft.GSC, False))  # Export Option 4
        self.export_all_channels.add_command(label="NetCDF", state=tk.DISABLED)

        self.export_type_dropdown.add_cascade(label="Export one channel", menu=self.export_one_channel)
        self.export_one_channel.add_command(label="NTGS Database", state=tk.DISABLED,
                                            command=partial(self.saveFile, ft.NTGS, True))  # Export Option 0
        self.export_one_channel.add_command(label="GeoPrecision (GP5W CSV)", state=tk.DISABLED,
                                            command=partial(self.saveFile, ft.GP5W, True))  # Export Option 1
        self.export_one_channel.add_command(label="GeoPrecision (FG2 CSV)", state=tk.DISABLED,
                                            command=partial(self.saveFile, ft.FG2, True))  # Export Option 2
        self.export_one_channel.add_command(label="GTN-P Database", state=tk.DISABLED,
                                            command=partial(self.saveFile, ft.GTNP, True))  # Export Option 3
        self.export_one_channel.add_command(label="GSC cleaned raw", state=tk.DISABLED,
                                            command=partial(self.saveFile, ft.GSC, True))  # Export Option 4
        self.export_one_channel.add_command(label="NetCDF", state=tk.DISABLED)

        self.file_dropdown.add_cascade(label="Open Data Source", menu=self.file_open_type_dropdown)
        self.file_dropdown.add_command(label="Close Data Source", command=self.closeFile)
        self.file_dropdown.add_separator()
        self.file_dropdown.add_command(label="Apply Changes From Log File", state=tk.DISABLED, command=self.importLog)
        self.file_dropdown.add_separator()
        self.file_dropdown.add_cascade(label="Export Changes", menu=self.export_type_dropdown)
        self.file_dropdown.add_separator()
        self.file_dropdown.add_command(label="View file metadata", command=self.showMetadata)
        self.file_dropdown.add_command(label="Quit", command=parent.quit)

        self.program_menu.add_cascade(label="File", menu=self.file_dropdown)
        self.program_menu.add_cascade(label="Filters", menu=self.filters_dropdown)
        self.program_menu.add_cascade(label="Depths", menu=self.depths_dropdown)
        self.program_menu.add_cascade(label="Context data", menu=self.context_data_dropdown)
        self.program_menu.add_cascade(label="Help!", menu=self.help_dropdown)

        # option 0
        self.context_data_dropdown.add_cascade(label="Add context data", menu=self.context_data_type_dropdown,
                                               state=tk.DISABLED)
        self.context_data_type_dropdown.add_command(label="NTGS Database (CSV)",
                                                    command=partial(self.openFile, ft.NTGS, True))
        self.context_data_type_dropdown.add_command(label="GSC cleaned raw (CSV)",
                                                    command=partial(self.openFile, ft.GSC, True))
        # option 1
        self.context_data_dropdown.add_command(label="Show context data", command=self.show_context_data,
                                               state=tk.DISABLED)
        # option 2
        self.context_data_dropdown.add_command(label="Hide context data", command=self.hide_context_data,
                                               state=tk.DISABLED)

        self.help_dropdown.add_command(label="How to Use", command=self.showHelp)
        self.help_dropdown.add_command(label="About", command=self.showAbout)
        self.help_dropdown.add_command(label="Open Demo Dataset", command=self.demoFile)

        self.parent.config(menu=self.program_menu)

    def loadDepths(self, depths):
        self.depthStates = []
        for depth in depths:
            func = self.getToggleFunction(depth)
            state = tk.BooleanVar()
            state.set(True)
            self.depths_dropdown.add_checkbutton(label=depth, indicatoron=True, variable=state, command=func)
            self.depthStates.append(state)
        self.depths_dropdown.add_command(label="View all", command=self.toggleAllDepths)
        self.depths_dropdown.add_command(label="View none", command=self.toggleNoDepths)
        self.depths_dropdown.add_command(label="Change Depths", command=self.changeDepths)

    def changeDepths(self):
        self.toolbarObservable.callbacks["renameDepths"]()

    def getToggleFunction(self, depth):
        def toggle():
            self.toolbarObservable.callbacks["toggleDepth"](depth)

        return toggle

    def toggleAllDepths(self):
        for state in self.depthStates:
            state.set(True)
        self.toolbarObservable.callbacks["toggleAllDepths"]()

    def toggleNoDepths(self):
        for state in self.depthStates:
            state.set(False)
        self.toolbarObservable.callbacks["toggleNoDepths"]()

    def showAbout(self):
        AboutDialog(self.parent)

    def showHelp(self):
        HelpDialog(self.parent)

    def showMetadata(self):
        text = self.toolbarObservable.callbacks["getMetadata"]()
        MetadataDialog(self.parent, text)

    def getFilterConfiguration(self, filter):
        confirmed_values = FilterConfigure(self.parent).display(filter.getName(), filter.getParams(),
                                                                filter.getHelpText())
        if confirmed_values:
            new_filter = FilterContainer(filter.getName(), filter.getFunction(), filter.getParams(),
                                         filter.getHelpText())
            new_filter.setUserParams(confirmed_values)
            self.toolbarObservable.callbacks["addFilter"](new_filter)

    def loadFilters(self, filters):
        for filter in filters:
            self.filters_dropdown.add_command(label=filter.getName(),
                                              command=partial(self.getFilterConfiguration, filter))

    def clearFilters(self):
        self.filters_dropdown.delete(0, self.filters_dropdown.index(tk.END) + 1 if self.filters_dropdown.index(
            tk.END) else 0)

    def clearDepths(self):
        self.depths_dropdown.delete(0,
                                    self.depths_dropdown.index(tk.END) + 1 if self.depths_dropdown.index(tk.END) else 0)

    def toggleExports(self, tkState, fileType=None):
        if fileType is None:
            # Likely that this runs when you want to disable all export options.
            menuLength = self.export_all_channels.index(tk.END)
            for index in range(menuLength + 1):
                self.export_all_channels.entryconfigure(index, state=tkState)
                self.export_one_channel.entryconfigure(index, state=tkState)
        else:
            for index in self._FULL_DATA_OPTIONS[fileType]:
                self.export_all_channels.entryconfigure(index, state=tkState)
            for index in self._SINGLE_CHANNEL_OPTIONS[fileType]:
                self.export_one_channel.entryconfigure(index, state=tkState)

    def saveFile(self, exportType, single_channel):
        init_name = re.sub(r"([^\.]+)\.(.*)$", r"\1_export.\2",
                           Path(self.filepath.get()[0]).name) if self.filepath.get() else None
        init_dir = Path(self.exportPath.get()).parent if self.exportPath.get() else None
        exportPath = filedialog.asksaveasfilename(
            parent=self.parent,
            title="Export Data",
            initialdir=init_dir,
            initialfile=init_name,
            filetypes=[("CSV Files", "*.csv"),
                       ("Text Files", "*.txt"),
                       ("All files", "*"), ]
        )
        if exportPath:
            self.exportPath.callbacks["exportData"](exportPath, exportType, single_channel)
            self.exportPath.data = exportPath
        else:
            return

    def importLog(self):
        logPath = filedialog.askopenfilename(
            parent=self.parent,
            title="Import Log",
            filetypes=[("Log Files", "*.log"), ("All Files", "*")]
        )
        if logPath:
            self.toolbarObservable.callbacks["importLog"](logPath)
        else:
            return

    def openFile(self, dataType, unselectable=False):
        init_dir = Path(self.filepath.get()[0]).parent if self.filepath.get() else None
        filetypes = ft.file_open_extensions(dataType)
        filepath = filedialog.askopenfilename(
            parent=self.parent,
            initialdir=init_dir,
            title="Select Dataset",
            filetypes=filetypes
        )
        if filepath:
            self.doOpenFile(dataType, filepath, unselectable)
        else:
            return

    def doOpenFile(self, dataType, filepath, unselectable=False):
        if self.filepath.get() and not unselectable:
            self.closeFile()
        if unselectable:
            self.toolbarObservable.callbacks["set_unselectable_import_type"](dataType)
        else:
            self.toolbarObservable.callbacks["setImportType"](dataType)
        self.toggleExports(tk.NORMAL, dataType)
        # tkinter is awful here, 3 is referring to the "Import Log File" option in the menu
        # (separators take up an index)
        self.file_dropdown.entryconfigure(3, state=tk.NORMAL)
        self.filepath.set((filepath, unselectable))

    def closeFile(self):
        self.file_dropdown.entryconfigure(3, state=tk.DISABLED)
        self.toggleExports(tk.DISABLED)
        self.filepath.set(-1)
        self.filepath.unset()

    def demoFile(self):
        self.doOpenFile("geoprecision", resourcePath("assets/demo_GP5W.txt"))

    def show_context_data(self):
        self.toolbarObservable.callbacks["show_context_data"]()
        self.toggle_context_data_options(tk.NORMAL, [2])
        self.toggle_context_data_options(tk.DISABLED, [1])
        return

    def hide_context_data(self):
        self.toolbarObservable.callbacks["hide_context_data"]()
        self.toggle_context_data_options(tk.NORMAL, [1])
        self.toggle_context_data_options(tk.DISABLED, [2])
        return

    def toggle_context_data_options(self, state, indices: list = None):
        if indices is None:
            menu_length = self.context_data_dropdown.index(tk.END)
            indices = range(menu_length + 1)
        for index in indices:
            self.context_data_dropdown.entryconfigure(index, state=state)
        return
