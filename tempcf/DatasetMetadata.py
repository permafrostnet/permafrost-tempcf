from typing import Union
from pprint import pformat
import re
import codecs

class DatasetMetadata():
    def __init__(self, filepath):
        self._filepath = filepath
        self._metaFields = {'filepath': filepath}
        self._trueDepths = False
        self._depth_map = {}
        
    def setField(self, metaCol, metaColValue):
        self._metaFields[metaCol] = metaColValue
 
    def getField(self, metaKey):
        try:
            return self._metaFields[metaKey]
        except KeyError:
            return None
    
    def getAllFields(self):
        return self._metaFields.items()
    
    def setDepthMap(self, depths: "dict[Union[str,int,float], float]"):
        if not isinstance(depths, dict):
            raise ValueError("Depth map must be a dictionary")
        
        self._trueDepths = True
        self._depth_map = depths
        
    def getDepthMap(self) -> "dict[Union[str,int,float], float]":
        return self._depth_map
    
    def getLatitude(self):
        return self.getField("latitude")
        
    def setLatitude(self, latitude):
        self.setField("latitude", latitude)
    
    def getLongitude(self):
        return self.getField("longitude")
        
    def setLongitude(self, longitude):
        self.setField("longitude", longitude)
    
    def getProject(self):
        return self.getField("project")
        
    def setProject(self, project):
        self.setField("project", project)
    
    def getPlatformId(self):
        return self.getField("platform_id")
        
    def setPlatformId(self, platform_id):
        self.setField("platform_id", platform_id)

    def get_installation_code(self):
        return self.getField("installation_code")

    def set_installation_code(self, installation_code):
        return self.setField("installation_code", installation_code)

    def get_download_date(self):
        return self.getField("download date")

    def set_download_date(self, download_date):
        return self.setField("download date", download_date)

    def get_logger_sn(self):
        return self.getField("logger SN")

    def set_logger_sn(self, logger_sn):
        return self.setField("logger SN", logger_sn)

    def get_channel_num(self):
        return self.getField("channel_num")

    def set_channel_num(self, channel_num):
        return self.setField("channel_num", channel_num)

    def formatted(self):
        text = pformat(self._metaFields)
        return decode_escapes(text)


ESCAPE_SEQUENCE_RE = re.compile(r'''
    ( \\U........      # 8-digit hex escapes
    | \\u....          # 4-digit hex escapes
    | \\x..            # 2-digit hex escapes
    | \\[0-7]{1,3}     # Octal escapes
    | \\N\{[^}]+\}     # Unicode characters by name
    | \\[\\'"abfnrtv]  # Single-character escapes
    )''', re.UNICODE | re.VERBOSE)


def decode_escapes(s):
    def decode_match(match):
        return codecs.decode(match.group(0), 'unicode-escape')

    return ESCAPE_SEQUENCE_RE.sub(decode_match, s)
