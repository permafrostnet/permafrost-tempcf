#=========================================================================================#
################################### f.outlier_rm ##########################################
#=========================================================================================#
# This function removes obvious outliers using two very basic methods:
#  (1) Removing above and below max and min thresholds set by depth
#  (2) Removing based on standard deviation
# This function outputs a time series plot, with the original data in black overlain by the 
#  cleaned data in red
# These methods can easily be played around with to better match your dataset

f.outlier_rm <- function(tab) {
  tab_cln <- tab
  
  # (1) Remove the most obvious outliers based on depth
  for (i in names(tab[,2:ncol(tab)])){
    test <- tab[,i]
    
    if (grepl("p", i)) {
      test[which(test > 45 | test < -45 )] <- NA

    } else {
      dep <- as.numeric(unlist(strsplit(i, "m"))[2])
      
      if (dep > 0 & dep < 1) {
        test[which(test > 15 | test < -40 )] <- NA

      } else if (dep >= 1 & dep < 2.5) {
        test[which(test > 30 | test < -35 )] <- NA

      } else if (dep >= 2.5 & dep < 5) {
        test[which(test > 22 | test < -20 )] <- NA

      } else if (dep >= 5) {
        test[which(test > 10 | test < -10 )] <- NA
        
      }
    }
    
    # (2) Remove outliers based on standard deviation
    std <- sd(test, na.rm=TRUE)
    
    test[which(test > mean(test, na.rm=TRUE) + 7*(sqrt(std))^1.3)] <- NA
    test[which(test < mean(test, na.rm=TRUE) - 7*(sqrt(std))^1.3)] <- NA
    
    # Plot
    # plot tab (has not been modified)
    plot(tab$time, tab[,i], type="l", ylim=c(min(tab[,i], na.rm=TRUE), max(tab[,i], na.rm=TRUE)), main=i)
    par(new=T)
    plot(tab$time, test, type="l", ylim=c(min(tab[,i], na.rm=TRUE), max(tab[,i], na.rm=TRUE)), col="red")
    
    
    # Replace in dataframe
    tab_cln[,i] <- test
  }
  rownames(tab_cln) <- 1:nrow(tab_cln)
  return(tab_cln)
}

